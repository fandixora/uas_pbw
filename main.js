window.addEventListener('load', ()=>{
    const form= document.querySelector("#task-form");
    const input= document.querySelector("#task-input");
    const list= document.querySelector("#tasks");
    const input_date= document.querySelector("#task-input-date");

    form.addEventListener('submit', (e)=>{
        e.preventDefault();

        const task = input.value;
        

        const task_date = input_date.value;
        if (!task) {
            return;
        } 
        if (!task_date) {
            return;
        } 


       

        const task_div = document.createElement("tr");
        const td_teks = document.createElement("td");
        const td_date = document.createElement("td");
        const td_edit = document.createElement("td");
        const td_selesai = document.createElement("td");
        const td_delete = document.createElement("td");

   

        const task_input= document.createElement("input");
        task_input.classList.add("text");
        task_input.type = "text";
        task_input.value= task;
        task_input.setAttribute("readonly", "readonly");
        td_teks.appendChild(task_input);
        task_div.appendChild(td_teks);

        
        const task_input_date= document.createElement("input");
        task_input_date.classList.add("task-input-date");
        task_input_date.type = "text_date";
        task_input_date.value= task_date;
        task_input_date.setAttribute("readonly", "readonly");
        td_date.appendChild(task_input_date);
        task_div.appendChild(td_date);





        const task_edit_botton= document.createElement("button");
        task_edit_botton.classList.add("Edit");
        task_edit_botton.innerHTML = "Edit";

        const task_delete_button= document.createElement("button");
        task_delete_button.classList.add("Delete");
        task_delete_button.innerHTML = "Hapus";

        const task_completed_button= document.createElement("button");
        task_completed_button.classList.add("Completed");
        task_completed_button.innerHTML = "Selesai";

        td_edit.appendChild(task_edit_botton);
        task_div.appendChild(td_edit);
        td_selesai.appendChild(task_completed_button);
        task_div.appendChild(td_selesai);
        td_delete.appendChild(task_delete_button);
        task_div.appendChild(td_delete);
        list.appendChild(task_div);
        

        task_edit_botton.addEventListener('click', ()=>{
            
            if (task_edit_botton.innerText.toLowerCase() =="edit") {
                    task_input.removeAttribute("readonly");
                    task_input.focus();
                    task_input_date.removeAttribute("readonly");
                    task_input_date.focus();
                    task_edit_botton.innerText = "Simpan";
                    task_input.style.textDecoration="none"
            }else{
                task_input.setAttribute("readonly", "readonly");
                task_input_date.setAttribute("readonly", "readonly");
                task_edit_botton.innerText ="Edit";
                
            }
        });

        task_delete_button.addEventListener('click', ()=>{
            if (confirm("apakah kamu yakin ingin menghapus pesan nya?")) {
                list.removeChild(task_div);
                

            }
        })
        
        task_completed_button.addEventListener('click', ()=>{
            
                task_input.style.textDecoration="line-through";
                task_input.setAttribute("readonly", "readonly");
               
        })

   
        input.value = "";


    });
});